import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Main from './src/pages/Main';
import 'react-native-gesture-handler';
const Stack = createNativeStackNavigator();

const App: React.FunctionComponent = () => (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        options={{headerShown: false}}
        name={'Main'}
        component={Main}
      />
    </Stack.Navigator>
  </NavigationContainer>
);

export default App;
