import {PROFILE_DATA} from '../types/profileTypes';

const initialState = {profileData: []};

const checkInReducer = (proIn = initialState, action: any) => {
  const {type, payload} = action;

  switch (type) {
    case PROFILE_DATA:
      return {...proIn, profileData: payload};

    default:
      return {...proIn};
  }
};

export default checkInReducer;
