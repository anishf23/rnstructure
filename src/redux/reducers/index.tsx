import {combineReducers} from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistReducer} from 'redux-persist';
import checkIn from './checkInReducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  profileList: ['profileData'],
};

export default combineReducers({
  checkIn: persistReducer(persistConfig, checkIn),
});
