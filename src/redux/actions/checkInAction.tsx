import {PROFILE_DATA} from '../types/profileTypes';
import * as data from '../../pages/Home/data';
import {AppLog} from '../../utils/common';

const getProfileList = () => async (dispatch: any) => {
  try {
    const dataResponse = await data.callGetApi(data.movieListUrl);
    dispatch({
      type: PROFILE_DATA,
      payload: dataResponse.movies,
    });
  } catch (err) {
    AppLog(err);
  }
};

export {getProfileList};
