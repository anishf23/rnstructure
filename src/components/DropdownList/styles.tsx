import {StyleSheet, Platform} from 'react-native';
import {FontSizes, Colors, wp, hp, DeviceHeight} from '../../theme';

const styles = StyleSheet.create({
  mainContainer: {
    height: hp(75),
    width: wp(90),
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    flexDirection: 'column',
    backgroundColor: Colors.white,
    borderWidth: 1,
    borderColor: Colors.border,
    borderRadius: 7,
    marginTop: DeviceHeight / 7,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 7,
    elevation: 5,
  },
  headerText: {
    flex: 1,
    width: '100%',
    color: Colors.theme,
    fontSize: FontSizes.xxLarge,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 5,
  },
  headerIcon: {
    width: wp(7),
    height: hp(7),
    alignItems: 'center',
    tintColor: Colors.theme,
    marginEnd: 15,
  },
  itemRow: {
    height: 50,
    width: wp(90),
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },
  itemInnerView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    flex: 1,
    color: Colors.black,
    width: wp(100),
    height: 50,
    fontSize: FontSizes.xLarge,
    textAlign: 'left',
    paddingTop: 13,
    marginEnd: 50,
    marginStart: 15,
  },
  lineView: {
    height: 0.8,
    width: '100%',
    backgroundColor: Colors.black,
    opacity: 0.2,
  },
  headerTitleView: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerBottmLineView: {
    height: 0.8,
    width: '100%',
    backgroundColor: Colors.black,
    opacity: 0.2,
    marginTop: 10,
  },
});

export default styles;
