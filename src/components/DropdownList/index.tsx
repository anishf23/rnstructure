/* eslint-disable react-native/no-inline-styles */
import React, {FC, ReactNode} from 'react';
import {
  Text,
  Modal,
  FlatList,
  TouchableOpacity,
  View,
  Image,
  Alert,
} from 'react-native';
import styles from './styles';
import {
  Colors,
  DeviceHeight,
  DeviceWidth,
  FontSizes,
  Images,
  hp,
  wp,
} from '../../theme';
interface DropdownListProps {
  title?: string;
  selected?: any;
  id?: string;
  viewStyle?: any;
  isvisible?: any;
  headerTitle?: string;
  Data: any;
  onItemClick: any;
  closeClick: any;
}
interface ItemProps {
  item: any;
}

const DropdownList: FC<DropdownListProps> = ({
  viewStyle,
  isvisible,
  headerTitle,
  Data,
  onItemClick,
  closeClick,
}) => {
  const renderItem = ({item}: {item: DropdownListProps}) => {
    return (
      <TouchableOpacity
        style={styles.itemRow}
        onPress={() => onItemClick(item)}>
        <View style={styles.itemInnerView}>
          <Text numberOfLines={1} ellipsizeMode="tail" style={styles.titleText}>
            {item.title}
          </Text>
          {item.selected ? (
            <Image
              source={Images.headerHomeIcon}
              style={styles.headerIcon}
              resizeMode="contain"
            />
          ) : null}
        </View>
        <View style={styles.lineView} />
      </TouchableOpacity>
    );
  };
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isvisible}
      onRequestClose={closeClick}>
      <View style={[styles.mainContainer, viewStyle]}>
        <View style={styles.headerTitleView}>
          <View style={styles.headerIcon} />
          <Text style={styles.headerText}>{headerTitle}</Text>
          <TouchableOpacity onPress={closeClick}>
            <Image
              source={Images.headerHomeIcon}
              style={styles.headerIcon}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.headerBottmLineView} />
        <FlatList
          data={Data}
          showsVerticalScrollIndicator={false}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    </Modal>
  );
};

export default DropdownList;
