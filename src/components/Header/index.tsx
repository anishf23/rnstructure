import React, {FC} from 'react';
import {Text, Image, TouchableOpacity, View} from 'react-native';
import styles from './styles';

interface HeaderProps {
  viewStyle?: any;
  headerTitle: string;
  headerLeftIcon: any;
  headerRightIcon: any;
  onLeftIconPress: any;
  onRightIconPress: any;
}

const Header: FC<HeaderProps> = ({
  viewStyle,
  onLeftIconPress,
  headerLeftIcon,
  headerTitle,
  onRightIconPress,
  headerRightIcon,
}) => {
  return (
    <View style={[styles.mainContainer, viewStyle]}>
      <TouchableOpacity onPress={onLeftIconPress}>
        <Image
          style={[styles.headerIcon]}
          resizeMode="contain"
          source={headerLeftIcon}
        />
      </TouchableOpacity>

      <Text style={styles.headerText}>{headerTitle}</Text>

      <TouchableOpacity onPress={onRightIconPress}>
        <Image
          style={[styles.headerIcon]}
          resizeMode="contain"
          source={headerRightIcon}
        />
      </TouchableOpacity>
    </View>
  );
};

export default Header;
