import {StyleSheet, Platform} from 'react-native';
import {FontSizes, Colors, wp, hp} from '../../theme';

const styles = StyleSheet.create({
  mainContainer: {
    height: hp(10),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: Colors.theme,
    marginTop: Platform.OS === 'android' ? 23 : 10,
  },
  headerText: {
    flex: 1,
    color: Colors.white,
    fontSize: FontSizes.xxLarge,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  headerIcon: {
    width: wp(8),
    height: hp(8),
    alignItems: 'center',
    tintColor: Colors.white,
    margin: 10,
  },
});

export default styles;
