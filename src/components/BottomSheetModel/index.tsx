import React, {FC, useRef, useMemo, useCallback} from 'react';
import {Text, Image, TouchableOpacity, View} from 'react-native';
import styles from './styles';
import BottomSheet from '@gorhom/bottom-sheet';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

interface BottomSheetProps {
  isvisible?: any;
}

const BottomSheetModel: FC<BottomSheetProps> = ({isvisible}) => {
  // ref
  const bottomSheetRef = useRef<BottomSheet>(null);

  // variables
  const snapPoints = useMemo(() => ['25%', '100%'], []);

  // callbacks
  const handleSheetChanges = useCallback((index: number) => {
    console.log('handleSheetChanges', index);
  }, []);

  return (
    <View
      style={[
        styles.container,
        {
          display: isvisible ? 'flex' : 'none',
          backgroundColor: 'rgba(52, 52, 52, 0.2)',
        },
      ]}>
      <GestureHandlerRootView style={{flex: 1}}>
        <BottomSheet
          ref={bottomSheetRef}
          index={1}
          snapPoints={snapPoints}
          onChange={handleSheetChanges}>
          <View style={styles.contentContainer}>
            <Text style={{color: 'red'}}>Awesome 🎉</Text>
            <Text style={{color: 'red'}}>Awesome 🎉</Text>
            <Text style={{color: 'red'}}>Awesome 🎉</Text>
            <Text style={{color: 'red'}}>Awesome 🎉</Text>
            <Text style={{color: 'red'}}>Awesome 🎉</Text>
            <Text style={{color: 'red'}}>Awesome 🎉</Text>
            <Text style={{color: 'red'}}>Awesome 🎉</Text>
            <Text style={{color: 'red'}}>Awesome 🎉</Text>
            <Text style={{color: 'red'}}>Awesome 🎉</Text>
          </View>
        </BottomSheet>
      </GestureHandlerRootView>
    </View>
  );
};

export default BottomSheetModel;
