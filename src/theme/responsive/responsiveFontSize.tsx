/* eslint-disable no-nested-ternary */
import {Dimensions, Platform, StatusBar} from 'react-native';
import {isIphoneX} from './iPhonexHelper';

const {height, width} = Dimensions.get('window');
const standardLength = width > height ? width : height;
const getOffset = () => {
  if (width > height) {
    return 0;
  }
  if (Platform.OS === 'ios') {
    return 78;
  }
  return StatusBar.currentHeight;
};
const offset = getOffset(); // iPhone X style SafeAreaView size in portrait

const deviceHeight =
  isIphoneX() || Platform.OS === 'android'
    ? standardLength - offset
    : standardLength;

export function RFPercentage(percent: number) {
  const heightPercent = (percent * deviceHeight) / 100;
  return Math.round(heightPercent);
}

// guideline height for standard 5" device screen is 680
export function RFValue(fontSize: number, standardScreenHeight = 680) {
  const heightPercent = (fontSize * deviceHeight) / standardScreenHeight;
  return Math.round(heightPercent);
}
