const Colors = {
  theme: '#D32D1F',
  white: '#FFFFFF',
  themeDark: '#7E170E',
  themeLight: '#E63223',
  black: '#000000',
  border: '#E7E7E7',
};

export default Colors;
