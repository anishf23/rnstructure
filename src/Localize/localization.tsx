import LocalizedStrings from 'react-localization';
import en from './en';
import de from './de';

const strings = new LocalizedStrings({
  en,
  de,
});
export default strings;
