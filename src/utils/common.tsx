import {Platform, PermissionsAndroid} from 'react-native';
import moment from 'moment';

export const GET = 'GET';
export const POST = 'POST';

const distance = (
  latValue1: number,
  latValue2: number,
  lonValue1: number,
  lonValue2: number,
) => {
  const lon1 = (lonValue1 * Math.PI) / 180;
  const lon2 = (lonValue2 * Math.PI) / 180;
  const lat1 = (latValue1 * Math.PI) / 180;
  const lat2 = (latValue2 * Math.PI) / 180;

  // Haversine formula
  const dlon = lon2 - lon1;
  const dlat = lat2 - lat1;
  const a =
    Math.sin(dlat / 2) ** 2 +
    Math.cos(lat1) * Math.cos(lat2) * Math.sin(dlon / 2) ** 2;

  const c = 2 * Math.asin(Math.sqrt(a));

  // const r = 6371;                     // for k.m.
  const r = 3956; // for miles

  // calculate the result
  return (c * r).toFixed(1);
};

const requestLocationPermission = async () => {
  if (Platform.OS === 'ios') {
    return true;
  }
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Location Access Required',
        message: 'This App needs to Access your location',
        buttonPositive: '',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    }
    return false;
  } catch (err) {
    AppLog(err, '');
  }
  return false;
};

const getFormatedTime = (time: moment.MomentInput) => {
  const formatedTime = moment(time).format('DD MMM HH:m');
  return formatedTime;
};
const AppLog = (rnlogtitle: unknown, rnlogtext?: string) => {
  if (__DEV__) {
    //log in dev build
    console.log(rnlogtitle, '' + rnlogtext);
  } else {
    //log in product build
  }
};
export {distance, requestLocationPermission, getFormatedTime, AppLog};
