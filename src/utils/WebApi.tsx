export const callAPI = async (
  url: RequestInfo,
  method: string,
  header: {Accept: string; 'Content-Type': string},
) => {
  try {
    const response = await fetch(url, {
      method: method,
      headers: header,
    });
    const responseData = await response.json();
    return responseData;
  } catch (error) {
    console.error(error);
    return error;
  }
};
