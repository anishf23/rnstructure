import AsyncStorage from '@react-native-async-storage/async-storage';
import {makePersistable} from 'mobx-persist-store';

const {makeObservable, action, observable} = require('mobx');

class MovieListStore {
  //A variable which holds data.
  movies: any[] = [];
  constructor() {
    //To make data observable by other component
    makeObservable(
      this,
      {
        saveMovies: action,
        movies: observable,
      },
      {autoBind: false},
    );

    // To save data after app kill
    makePersistable(this, {
      name: 'movieListPersistStore',
      properties: ['movies'],
      storage: AsyncStorage,
    });
  }

  // to update data
  saveMovies(data: any[]) {
    this.movies = data;
  }
}

export default new MovieListStore();
