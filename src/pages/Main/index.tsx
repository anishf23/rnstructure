/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-unstable-nested-components */
import React, {FC} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Image,
  TouchableOpacity,
  Text,
} from 'react-native';
import styles from './styles';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
import Dummy from '../Dummy';
import {BottomTabImages} from './data';
import {Colors} from '../../theme';
import Home from '../Home';
import DummyTest from '../DummyTest';

interface TabProps {
  state: any;
  descriptors: any;
  navigation: any;
}
const generateKey = (len: number | undefined) => {
  const charSet =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let randomString = '';
  for (let i = 0; i < len; i++) {
    const randomPoz = Math.floor(Math.random() * charSet.length);
    randomString += charSet.substring(randomPoz, randomPoz + 1);
  }
  return randomString;
};
const setTabIconTint = (isFocused: any, index: number) => {
  if (index === 2) {
    return '';
  } else if (isFocused) {
    return Colors.theme;
  } else if (!isFocused) {
    return Colors.themeDark;
  }
};
const HomeStack = () => (
  <Stack.Navigator>
    <Stack.Screen name="Home" component={Home} options={{headerShown: false}} />
    <Stack.Screen
      name="Dummy"
      component={Dummy}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="DummyTest"
      component={DummyTest}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

const Main: FC = () => {
  const MyTabBar: FC<TabProps> = ({state, descriptors, navigation}) => {
    return (
      <View style={styles.tabView}>
        {state.routes.map(
          (route: {key: string | number; name: any}, index: number) => {
            const {options} = descriptors[route.key];
            // const label =
            //   options.tabBarLabel !== undefined
            //     ? options.tabBarLabel
            //     : options.title !== undefined
            //     ? options.title
            //     : route.name;

            const isFocused = state.index === index;

            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                // The `merge: true` option makes sure that the params inside the tab screen are preserved
                navigation.navigate({name: route.name, merge: true});
              }
            };

            const onLongPress = () => {
              navigation.emit({
                type: 'tabLongPress',
                target: route.key,
              });
            };

            return (
              <TouchableOpacity
                key={generateKey(index)}
                accessibilityRole="button"
                accessibilityState={isFocused ? {selected: true} : {}}
                accessibilityLabel={options.tabBarAccessibilityLabel}
                testID={options.tabBarTestID}
                onPress={onPress}
                onLongPress={onLongPress}
                style={[
                  styles.tabItem,
                  {
                    marginBottom: index === 2 ? 10 : 0,
                  },
                ]}>
                <Image
                  style={[
                    styles.tabIcon,
                    {
                      tintColor: setTabIconTint(isFocused, index),
                      height: index === 2 ? 70 : 25,
                      width: index === 2 ? 60 : 25,
                      marginBottom: index === 2 ? -5 : 0,
                    },
                  ]}
                  resizeMode={'contain'}
                  source={BottomTabImages[index].icon}
                />
                <Text
                  style={[
                    styles.textTab,
                    {
                      color: isFocused ? Colors.theme : Colors.themeDark,
                      marginBottom: index === 2 ? 8 : 0,
                    },
                  ]}>
                  {BottomTabImages[index].tablabel}
                </Text>
              </TouchableOpacity>
            );
          },
        )}
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <StatusBar
        backgroundColor={'#FFFFFF'}
        barStyle="dark-content"
        translucent
      />
      <Tab.Navigator tabBar={props => <MyTabBar {...props} />}>
        <Tab.Screen
          name="HomeStack"
          component={HomeStack}
          options={{headerShown: false}}
        />
        <Tab.Screen
          name="Dummy"
          component={Dummy}
          options={{headerShown: false}}
        />
        <Tab.Screen
          name="Book Shipment"
          component={Dummy}
          options={{headerShown: false}}
        />
        <Tab.Screen
          name="Demo3"
          component={Dummy}
          options={{headerShown: false}}
        />
        <Tab.Screen
          name="Demo4"
          component={Dummy}
          options={{headerShown: false}}
        />
      </Tab.Navigator>
    </SafeAreaView>
  );
};
export default Main;
