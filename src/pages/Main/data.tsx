import {Images} from '../../theme';

export const BottomTabImages = [
  {icon: Images.headerHomeIcon, tablabel: 'Home'},
  {icon: Images.headerHomeIcon, tablabel: 'Demo'},
  {icon: Images.headerHomeIcon, tablabel: 'Demo\nDemo'},
  {icon: Images.headerHomeIcon, tablabel: 'Chat'},
  {icon: Images.headerHomeIcon, tablabel: 'Profile'},
];
