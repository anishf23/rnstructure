import {Dimensions, StyleSheet} from 'react-native';
import { FontSizes } from '../../theme';

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  sectionContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  titleText: {
    color: '#000',
    fontSize: 35,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  tabIcon: {
    width: 25,
    height: 25,
  },
  tabView: {
    flexDirection: 'row',
    height: 65,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  tabItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textTab: {
    fontSize: FontSizes.xSmall,
    textAlign: 'center',
  },
});

export default styles;
