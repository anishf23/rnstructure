import React, {FC} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
const DummyTest: FC = () => {
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <StatusBar
        backgroundColor={'#FFFFFF'}
        barStyle="dark-content"
        translucent
      />
      <View style={styles.sectionContainer}>
        <TouchableOpacity onPress={() => null}>
          <Text style={styles.titleText}>DummyTest</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
export default DummyTest;
