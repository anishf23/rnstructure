/* eslint-disable react-hooks/exhaustive-deps */
import React, {FC, useEffect} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Text,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import BottomSheetModel from '../../components/BottomSheetModel';

export type RootStackParamList = {
  DummyTest: undefined;
};
type ScreenProps = NativeStackScreenProps<RootStackParamList>;
const Dummy: FC<ScreenProps> = props => {
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <StatusBar
        backgroundColor={'#FFFFFF'}
        barStyle="dark-content"
        translucent
      />
      <View style={styles.sectionContainer}>
        <TouchableOpacity
          onPress={() => props.navigation.navigate('DummyTest')}>
          <Text style={styles.titleText}>Dummy</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
export default Dummy;
