import {StyleSheet} from 'react-native';
import {FontSizes, Colors, hp, wp} from '../../theme';

const styles = StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: Colors.white,
  },
  mainView: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    marginTop: 15,
  },
  rightArrowIcon: {
    width: wp(5),
    height: hp(5),
    alignItems: 'center',
  },
  titleText: {
    color: Colors.black,
    fontSize: FontSizes.xxLarge,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

export default styles;
