import {GET} from '../../utils/common';
import {callAPI} from '../../utils/WebApi';

// Static Data String
export const HeaderTitle = 'Demo Header';
export const HeaderSubTitle = 'Hello Miro';
export const demo = 'RNstructure';
export const movieListUrl = 'https://reactnative.dev/movies.json';

// POST Api Call
export const callPostApi = async (_urlStr: any, params: any) => {
  return fetch(movieListUrl, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(params),
  })
    .then(response => response.json())
    .then(_responseData => {})
    .catch(error => {
      console.error(error);
      return error;
    });
};
// GET Api Call
export async function callGetApi(urlStr: string) {
  try {
    const header = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    const response = await callAPI(urlStr, GET, header);
    return response;
  } catch (error) {
    console.error(error);
    return error;
  }
}
export const TempData = [
  {
    id: 'jcsvr',
    title: 'demo jcdsvdvsvr',
    selected: false,
  },
  {
    id: 'jcdsvdvsvr',
    title: 'demo jcdsvdvsvr',
    selected: false,
  },
  {
    id: 'jcsvrwrvr',
    title: 'demo jcdsvdvsvr',
    selected: false,
  },
  {
    id: 'jcsvrwwbgeewr',
    title: 'demojcvghyt567svrfbrtj',
    selected: false,
  },
  {
    id: 'jcsvrvwgrwg',
    title: 'demo jcdsvdvsvr',
    selected: false,
  },
  {
    id: 'jcsvrfbrtj',
    title: 'demo jcdsvdvsvr',
  },
  {
    id: 'jcsvrfb34rtj',
    title: 'demojcdsvdvsvr',
    selected: false,
  },
  {
    id: 'jcsfy768vrfbrtj',
    title: 'demo jcdsvdvsvr',
    selected: false,
  },
  {
    id: 'jcs536rgedbgvrfbrtj',
    title: 'demojcdsvdvsvr',
    selected: false,
  },
  {
    id: 'jcsvzdsf09jmrfbrtj',
    title: 'demojcvghyt567svrfbrtj',
    selected: false,
  },
  {
    id: 'jcsvrf12wesdrbrtj',
    title: 'demojcsvrfbrt0987uijklj',
    selected: false,
  },
  {
    id: 'jcvghyt567svrfbrtj',
    title:
      'demojcvghyt567svrfbrtj demojcvghyt567svrfbrtj demojcvghyt567svrfbrtj',
    selected: true,
  },
  {
    id: 'jcsvrfbrt0987uijklj',
    title: 'demojcsvrfbrt0987uijklj',
    selected: false,
  },
  {
    id: 'jcsvvfcdfrttttt677rfbrtj',
    title: 'demojcvghyt567svrfbrtj',
    selected: false,
  },
];
