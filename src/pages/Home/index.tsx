/* eslint-disable react-native/no-inline-styles */
import React, {FC, useEffect, useState} from 'react';
import {
  View,
  FlatList,
  SafeAreaView,
  StatusBar,
  Text,
  TouchableOpacity,
  useColorScheme,
  Dimensions,
} from 'react-native';
import {Images, Colors, FontSizes, wp} from '../../theme';
import {Header, DropdownList} from '../../components';
import styles from './styles';
import * as data from './data';
import {useDispatch, useSelector} from 'react-redux';
import {getProfileList} from '../../redux/actions/checkInAction';
import strings from '../../Localize/localization';
import {observer} from 'mobx-react';
import MovieListStore from '../../MobX/MovieListStore';
import {AppLog} from '../../utils/common';
import {RootState} from '../../redux/store';

//To force a particular language
//strings.setLanguage('de');
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import BottomSheetModel from '../../components/BottomSheetModel';
export type RootStackParamList = {
  Dummy: undefined;
};
type ScreenProps = NativeStackScreenProps<RootStackParamList>;
const Home: FC<ScreenProps> = props => {
  const {HeaderTitle, demo} = data;
  const dispatch = useDispatch<any>();
  const colorScheme = useColorScheme();
  //const {profileData} = useSelector(state => state.checkIn);
  //const {checkIn} = useSelector(state => state);
  const profileData = useSelector(
    (state: RootState) => state.checkIn.profileData,
  );
  const [isModalVisible, setisModalVisible] = useState(false);
  //data from redux
  AppLog('redux MoviesList===>', JSON.stringify(profileData));

  //data from mobx
  AppLog('mobx MoviesList===>', JSON.stringify(MovieListStore.movies));

  useEffect(() => {
    //following is redux
    if (profileData.length === 0) {
      dispatch(getProfileList());
      //following is mobX
      saveDataUsingMobX();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const saveDataUsingMobX = () => {
    data.callGetApi(data.movieListUrl).then(response => {
      MovieListStore.saveMovies(response.movies);
    });
  };

  interface ItemProps {
    item: any;
  }
  const renderItem: FC<ItemProps> = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => setisModalVisible(true)} //props.navigation.navigate('Dummy')
        style={{
          height: 60,
          width: Dimensions.get('window').width - 50,
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
          margin: 10,
          backgroundColor:
            colorScheme === 'light' ? Colors.theme : Colors.black,
        }}>
        <Text style={{color: Colors.white, fontSize: FontSizes.large}}>
          {item.title}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <StatusBar
        backgroundColor={Colors.white}
        barStyle="dark-content"
        translucent
      />
      <View style={styles.sectionContainer}>
        <DropdownList
          isvisible={isModalVisible}
          headerTitle={'Select Language'}
          Data={data.TempData}
          closeClick={() => setisModalVisible(false)}
          onItemClick={(item: {title: string}) =>
            AppLog('onItemClick...', item.title)
          }
        />
        <Header
          headerTitle={HeaderTitle}
          headerLeftIcon={Images.headerHomeIcon}
          headerRightIcon={Images.headerHomeIcon}
          onLeftIconPress={() => AppLog('onLeftIconPress pressed...')}
          onRightIconPress={() => AppLog('onRightIconPress pressed...')}
        />
        <View style={styles.mainView}>
          <Text style={styles.titleText}>{demo}</Text>
          {/* string localisation demo  */}
          <Text style={styles.titleText}>{strings.how}</Text>

          <FlatList
            data={profileData}
            renderItem={renderItem}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
      {/* BottomSheetModel */}
      {/* <BottomSheetModel isvisible={false} /> */}
    </SafeAreaView>
  );
};
export default observer(Home);
